﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Security.Web.Services.Mapping.Tests
{
    public class EFMappingTests
    {
        [Fact]
        public void ConfigurationIsValidTest()
        {
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile<EFMapping>());                

            configuration.AssertConfigurationIsValid();
        }
    }
}
