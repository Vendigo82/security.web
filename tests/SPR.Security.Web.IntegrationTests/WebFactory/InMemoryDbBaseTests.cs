﻿using Xunit.Abstractions;

namespace SPR.Security.Web.IntegrationTests.WebFactory;

public class InMemoryDbBaseTests : IClassFixture<InMemoryDbWebApplicationFactory>
{
    protected readonly InMemoryDbWebApplicationFactory factory;
    protected readonly ITestOutputHelper outputHelper;
    protected readonly HttpClient httpClient;

    public InMemoryDbBaseTests(InMemoryDbWebApplicationFactory factory, ITestOutputHelper outputHelper)
    {
        this.factory = factory;
        this.outputHelper = outputHelper;
        httpClient = factory.CreateClient();
    }
}
