﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SPR.Security.Web.DAL.Model;

namespace SPR.Security.Web.IntegrationTests.WebFactory;

public class InMemoryDbWebApplicationFactory : WebApplicationFactory<Startup>
{
    private readonly string InMemoryDbName = Guid.NewGuid().ToString();

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        base.ConfigureWebHost(builder);

        builder.ConfigureAppConfiguration((context, builder) => builder
            .AddJsonFile("appsettings.Testing.json", true)
            .AddInMemoryCollection(new Dictionary<string, string>
            {                
            })
            );


        builder.ConfigureTestServices(services =>
        {
            services.RemoveAll<DbContextOptions<GuardContext>>();
            services.AddDbContext<GuardContext>(optionsBuilder => optionsBuilder
                .UseInMemoryDatabase(InMemoryDbName, options => options.EnableNullChecks(false))
                .ConfigureWarnings(configurationBuilder => configurationBuilder.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                );
        });
    }
}
