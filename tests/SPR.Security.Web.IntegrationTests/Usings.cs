global using FluentAssertions;
global using SPR.Security.Web.IntegrationTests.WebFactory;
global using System.Net.Http.Json;
global using Xunit;
global using Xunit.Abstractions;
