﻿namespace SPR.Security.Web.IntegrationTests;

public class CommonEndpointsTests : InMemoryDbBaseTests
{
    public CommonEndpointsTests(InMemoryDbWebApplicationFactory factory, ITestOutputHelper outputHelper) : base(factory, outputHelper)
    {
    }

    [Fact]
    public async Task VersionEndpoint_ShouldBe200Ok()
    {
        var response = await httpClient.GetAsync("/version");

        response.Should().Be200Ok();
        var payload = await response.Content.ReadFromJsonAsync<VersionResponse>();
        payload!.Version.Should().Match("*.*.*.*");
    }

    [Theory]
    [InlineData("/metrics")]
    public async Task EndpointsShouldBe200Ok(string url)
    {
        var response = await httpClient.GetAsync(url);

        response.Should().Be200Ok();
    }

    private class VersionResponse
    {
        public string Version { get; set; } = null!;
    }
}
