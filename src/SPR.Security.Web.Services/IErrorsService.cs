﻿using SPR.Security.Web.Services.DataModel.Errors;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services
{
    public interface IErrorsService
    {
        /// <summary>
        /// Loading list or errors
        /// </summary>
        /// <param name="fromId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        Task<IList<ErrorTableModel>> LoadErrorsAsync(long? fromId, int count);

        /// <summary>
        /// Load error by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Item or null if not found</returns>
        Task<ErrorItemModel> LoadErrorAsync(long id);
    }
}
