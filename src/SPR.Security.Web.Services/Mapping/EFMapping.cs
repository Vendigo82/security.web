﻿using AutoMapper;
using SPR.Security.Web.DAL.Model;
using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Errors;
using SPR.Security.Web.Services.DataModel.Interactions;
using System.Linq;

namespace SPR.Security.Web.Services.Mapping
{
    public class EFMapping : Profile
    {
        public EFMapping()
        {
            CreateMap<Client, ClientBriefModel>();

            CreateMap<Client, ClientDataModel>()
                .ReverseMap()
                .ForMember(d => d.Id, m => m.Ignore())
                .ForMember(d => d.InsertedDate, m => m.Ignore());

            CreateMap<Client, ClientSummaryModel>()
                .IncludeBase<Client, ClientBriefModel>()
                .ForMember(d => d.LastUpdate, m => m.MapFrom(s => s.Tokens.OrderByDescending(i => i.Id).First().IssueDate))
                .ForMember(d => d.LastVerify, m => m.MapFrom(s => s.VerifyHistories.OrderByDescending(i => i.Id).First().InsertedDate))
                .ForMember(d => d.LastWarning, m => m.MapFrom(s => s.Warnings.OrderByDescending(i => i.Id).First()));

            CreateMap<Token, TokenModel>();
            CreateMap<VerifyHistory, VerifyHistoryModel>();

            CreateMap<Warning, WarningBriefModel>();
            CreateMap<Warning, WarningModel>()
                .IncludeBase<Warning, WarningBriefModel>();

            CreateMap<Error, ErrorTableModel>();

            CreateMap<Error, ErrorItemModel>()
                .IncludeBase<Error, ErrorTableModel>();
        }
    }
}
