﻿using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Interactions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services
{
    public interface IInteractionsService
    {
        Task<IList<TokenModel>> LoadTokensAsync(long? clientId, long? fromId, int count);

        Task<IList<VerifyHistoryModel>> LoadHistoryAsync(long? clientId, long? fromId, int count);

        Task<IList<WarningModel>> LoadWarningsAsync(long? clientId, long? fromId, int count);

        Task<IEnumerable<ClientSummaryModel>> LoadSummaryAsync();
    }
}
