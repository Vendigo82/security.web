﻿using SPR.Security.Web.Services.DataModel.Clients;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services
{
    public interface IClientCRUDService
    {
        Task<IList<ClientBriefModel>> GetClientsAsync();

        Task<ClientDataModel> GetClientAsync(long id);

        Task<bool> UpdateAsync(ClientDataModel model);

        Task<long> InsertAsync(ClientDataModel model);
    }
}
