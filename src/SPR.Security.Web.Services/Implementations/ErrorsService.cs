﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.Security.Web.DAL.Model;
using SPR.Security.Web.Services.DataModel.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services.Implementations
{
    public class ErrorsService : IErrorsService
    {
        private readonly GuardContext context;
        private readonly IMapper mapper;

        public ErrorsService(GuardContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IList<ErrorTableModel>> LoadErrorsAsync(long? fromId, int count)
        {
            var q = context.Errors.AsNoTracking().AsQueryable();
            if (fromId.HasValue)
                q = q.Where(i => i.Id < fromId.Value);

            return await q
                .OrderByDescending(i => i.Id)
                .Take(count)                
                .ProjectTo<ErrorTableModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();
        }

        public Task<ErrorItemModel> LoadErrorAsync(long id) => context
            .Errors
            .AsNoTracking()
            .Where(i => i.Id == id)
            .ProjectTo<ErrorItemModel>(mapper.ConfigurationProvider)
            .FirstOrDefaultAsync();
    }
}
