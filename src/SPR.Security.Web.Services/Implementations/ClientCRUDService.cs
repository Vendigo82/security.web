﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.Security.Web.DAL.Model;
using SPR.Security.Web.Services.DataModel.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services.Implementations
{
    public class ClientCRUDService : IClientCRUDService
    {
        private readonly GuardContext context;
        private readonly IMapper mapper;

        public ClientCRUDService(GuardContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IList<ClientBriefModel>> GetClientsAsync()
        {
            return await context.Clients.AsNoTracking().ProjectTo<ClientBriefModel>(mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<ClientDataModel> GetClientAsync(long id)
            => await context
            .Clients
            .AsNoTracking()
            .Where(i => i.Id == id)
            .ProjectTo<ClientDataModel>(mapper.ConfigurationProvider)
            .SingleOrDefaultAsync();

        public async Task<bool> UpdateAsync(ClientDataModel model)
        {
            var dbitem = await context.Clients.Where(i => i.Id == model.Id).SingleOrDefaultAsync();
            if (dbitem == null)
                return false;

            mapper.Map(model, dbitem);

            int cnt = await context.SaveChangesAsync();

            return cnt != 0;
        }

        public async Task<long> InsertAsync(ClientDataModel model)
        {
            var dbitem = mapper.Map<Client>(model);
            await context.Clients.AddAsync(dbitem);
            await context.SaveChangesAsync();
            return dbitem.Id;
        }
    }
}
