﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.Security.Web.DAL.Model;
using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Security.Web.Services.Implementations
{
    public class InteractionsService : IInteractionsService
    {
        private readonly GuardContext context;
        private readonly IMapper mapper;

        public InteractionsService(GuardContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IList<TokenModel>> LoadTokensAsync(long? clientId, long? fromId, int count)
        {
            var q = context.Tokens.AsQueryable().AsNoTracking();
            if (clientId != null)
                q = q.Where(i => i.ClientId == clientId.Value);
            if (fromId != null)
                q = q.Where(i => i.Id < fromId);

            var list = await q
                .OrderByDescending(i => i.Id)
                .Take(count)
                .ProjectTo<TokenModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();

            return list;
        }

        public async Task<IList<VerifyHistoryModel>> LoadHistoryAsync(long? clientId, long? fromId, int count)
        {
            var q = context.VerifyHistories.AsQueryable().AsNoTracking();
            if (clientId != null)
                q = q.Where(i => i.ClientId == clientId.Value);
            if (fromId != null)
                q = q.Where(i => i.Id < fromId);

            var list = await q
                .OrderByDescending(i => i.Id)
                .Take(count)
                .ProjectTo<VerifyHistoryModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();

            return list;
        }

        public async Task<IList<WarningModel>> LoadWarningsAsync(long? clientId, long? fromId, int count)
        {
            var q = context.Warnings.AsQueryable().AsNoTracking();
            if (clientId != null)
                q = q.Where(i => i.ClientId == clientId.Value);
            if (fromId != null)
                q = q.Where(i => i.Id < fromId);

            var list = await q
                .OrderByDescending(i => i.Id)
                .Take(count)
                .ProjectTo<WarningModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();

            return list;
        }

        public async Task<IEnumerable<ClientSummaryModel>> LoadSummaryAsync()
        {
            return await context.Clients.AsNoTracking()
                .ProjectTo<ClientSummaryModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();
        }
    }
}
