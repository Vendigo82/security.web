﻿using SPR.Security.Web.Services.DataModel.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Interactions
{
    public class VerifyHistoryModel
    {
        public long Id { get; set; }

        public ClientBriefModel Client { get; set; }

        [Display(Name = "Verify date")]
        public DateTime InsertedDate { get; set; }
    }
}
