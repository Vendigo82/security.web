﻿using SPR.Security.Web.Services.DataModel.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Interactions
{
    public class TokenModel
    {
        public long Id { get; set; }

        public ClientBriefModel Client { get; set; }

        [Display(Name = "Version")]
        public int Ver { get; set; }

        public string IpAddress { get; set; }

        public string MacAddress { get; set; }

        public DateTime IssueDate { get; set; }

        public int ExpireIn { get; set; }

        public int? VerifyToken { get; set; }
    }
}
