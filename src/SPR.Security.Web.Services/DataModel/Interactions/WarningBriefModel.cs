﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Interactions
{
    public class WarningBriefModel
    {
        public long Id { get; set; }

        [Display(Name = "Date")]
        public DateTime InsertedDate { get; set; }

        public string Text { get; set; }
    }
}
