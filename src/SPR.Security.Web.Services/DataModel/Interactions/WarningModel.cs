﻿using SPR.Security.Web.Services.DataModel.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Interactions
{
    public class WarningModel : WarningBriefModel
    {
        public ClientBriefModel Client { get; set; }

        public long VerifyId { get; set; }

        public int SourceId { get; set; }
    }
}
