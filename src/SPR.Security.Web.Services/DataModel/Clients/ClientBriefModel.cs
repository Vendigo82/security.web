﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Clients
{
    public class ClientBriefModel
    {
        public long Id { get; set; }

        [Display(Name = "Client name")]
        [StringLength(250, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [Required]
        public bool Enabled { get; set; }
    }
}
