﻿using SPR.Security.Web.Services.DataModel.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Clients
{
    public class ClientSummaryModel : ClientBriefModel
    {
        public DateTime? LastUpdate { get; set; }

        public DateTime? LastVerify { get; set; }

        public WarningBriefModel LastWarning { get; set; }
    }
}
