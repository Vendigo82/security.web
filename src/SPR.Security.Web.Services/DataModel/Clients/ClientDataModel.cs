﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SPR.Security.Web.Services.DataModel.Clients
{
    public class ClientDataModel : ClientBriefModel
    {
        public DateTime InsertedDate { get; set; }

        [StringLength(250, MinimumLength = 8)]
        [Required]
        public string Key { get; set; }

        public int? VerifyToken { get; set; }
    }
}
