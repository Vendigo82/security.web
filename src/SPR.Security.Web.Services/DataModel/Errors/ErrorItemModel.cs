﻿
namespace SPR.Security.Web.Services.DataModel.Errors
{
    public class ErrorItemModel : ErrorTableModel
    {
        public string Request { get; set; }

        public string Trace { get; set; }
    }
}
