﻿using System;

namespace SPR.Security.Web.Services.DataModel.Errors
{
    public class ErrorTableModel
    {
        public long Id { get; set; }

        public DateTime InsertedDate { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }
    }
}
