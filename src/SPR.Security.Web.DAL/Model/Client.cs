﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class Client
    {
        public Client()
        {
            Tokens = new HashSet<Token>();
            VerifyHistories = new HashSet<VerifyHistory>();
            Warnings = new HashSet<Warning>();
        }

        public long Id { get; set; }
        public DateTime InsertedDate { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public bool? Enabled { get; set; }
        public int? VerifyToken { get; set; }

        public virtual ICollection<Token> Tokens { get; set; }
        public virtual ICollection<VerifyHistory> VerifyHistories { get; set; }
        public virtual ICollection<Warning> Warnings { get; set; }
    }
}
