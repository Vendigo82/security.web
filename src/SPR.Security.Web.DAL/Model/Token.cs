﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class Token
    {
        public long Id { get; set; }
        public long ClientId { get; set; }
        public int Ver { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public DateTime IssueDate { get; set; }
        public int ExpireIn { get; set; }
        public int? VerifyToken { get; set; }
        public string Token1 { get; set; }
        public string Hash { get; set; }

        public virtual Client Client { get; set; }
    }
}
