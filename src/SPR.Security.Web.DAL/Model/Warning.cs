﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class Warning
    {
        public long Id { get; set; }
        public DateTime InsertedDate { get; set; }
        public long? ClientId { get; set; }
        public long? VerifyId { get; set; }
        public string Text { get; set; }
        public int SourceId { get; set; }

        public virtual Client Client { get; set; }
        public virtual VerifyHistory Verify { get; set; }
    }
}
