﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class GuardContext : DbContext
    {
        public GuardContext()
        {
        }

        public GuardContext(DbContextOptions<GuardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Error> Errors { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<VerifyHistory> VerifyHistories { get; set; }
        public virtual DbSet<Warning> Warnings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("data source=VENDIGOPC;initial catalog=SPRGuard;persist security info=True;user id=sa;password=123;");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("Client", "SPR");

                entity.HasIndex(e => e.Key, "UNQ__SPRClient__Key")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "UNQ__SPRClient__Name")
                    .IsUnique();

                entity.Property(e => e.Enabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.InsertedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Error>(entity =>
            {
                entity.ToTable("Error");

                entity.Property(e => e.InsertedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Message).HasMaxLength(4000);

                entity.Property(e => e.Name).HasMaxLength(1000);

                entity.Property(e => e.Request).HasMaxLength(4000);
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.ToTable("Token", "SPR");

                entity.HasIndex(e => e.ClientId, "IDX__Token__ClientId");

                entity.Property(e => e.Hash)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.IpAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.IssueDate).HasColumnType("datetime");

                entity.Property(e => e.MacAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Token1)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .HasColumnName("Token");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Tokens)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__SPRToken_ClientId");
            });

            modelBuilder.Entity<VerifyHistory>(entity =>
            {
                entity.ToTable("VerifyHistory", "SPR");

                entity.HasIndex(e => e.ClientId, "IDX__VerifyHistory__ClientId");

                entity.Property(e => e.InsertedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.VerifyHistories)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__VerifyHistory__Client");
            });

            modelBuilder.Entity<Warning>(entity =>
            {
                entity.ToTable("Warning", "SPR");

                entity.HasIndex(e => e.ClientId, "IDX__Warning__ClientId");

                entity.HasIndex(e => e.VerifyId, "IDX__Warning__VerifyId");

                entity.Property(e => e.InsertedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Text).IsRequired();

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Warnings)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK__SPRWarning_ClientId");

                entity.HasOne(d => d.Verify)
                    .WithMany(p => p.Warnings)
                    .HasForeignKey(d => d.VerifyId)
                    .HasConstraintName("FK__SPRWarning_VerifyId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
