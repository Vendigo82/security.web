﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class VerifyHistory
    {
        public VerifyHistory()
        {
            Warnings = new HashSet<Warning>();
        }

        public long Id { get; set; }
        public DateTime InsertedDate { get; set; }
        public long ClientId { get; set; }

        public virtual Client Client { get; set; }
        public virtual ICollection<Warning> Warnings { get; set; }
    }
}
