﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.Security.Web.DAL.Model
{
    public partial class Error
    {
        public long Id { get; set; }
        public DateTime InsertedDate { get; set; }
        public string Request { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string Trace { get; set; }
    }
}
