using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using SPR.Security.Web.Services;
using SPR.Security.Web.Settings;

namespace SPR.Security.Web.Pages.Errors
{
    public class IndexModel : PageModel
    {
        private readonly IErrorsService service;
        private readonly IOptions<PageSettings> settings;

        public IndexModel(IErrorsService service, IOptions<PageSettings> settings)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public IEnumerable<Services.DataModel.Errors.ErrorTableModel> Errors => Enumerable.Empty<Services.DataModel.Errors.ErrorTableModel>();

        public void OnGet()
        {            
        }

        public async Task<IActionResult> OnGetListAsync(long? id)
        {
            var list = await service.LoadErrorsAsync(id, settings.Value.ItemsPerPage);
            return Partial("_ErrorsTableRows", (list.AsEnumerable(), list.Count < settings.Value.ItemsPerPage));
        }

        public async Task<IActionResult> OnGetItemAsync(long id)
        {
            var item = await service.LoadErrorAsync(id);
            if (item == null)
                return NotFound();

            return Partial("_ErrorDetails", item);
        }
    }
}
