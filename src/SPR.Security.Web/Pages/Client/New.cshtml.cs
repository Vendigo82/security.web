using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;

namespace SPR.Security.Web.Pages.Client
{
    public class NewModel : PageModel
    {
        private readonly IClientCRUDService clientService;

        public NewModel(IClientCRUDService clientService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        [BindProperty]
        public ClientDataModel Client { get; set; }

        public IActionResult OnGet()
        {
            Client = new ClientDataModel {
                Enabled = true
            };
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            var id = await clientService.InsertAsync(Client);

            return RedirectToPage("Edit", new { id });
        }
    }
}
