using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;

namespace SPR.Security.Web.Pages.Client
{
    public class IndexModel : PageModel
    {
        private readonly IClientCRUDService clientService;

        public IndexModel(IClientCRUDService clientService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        public IList<ClientBriefModel> Clients { get; private set; }

        public async Task OnGetAsync()
        {
            Clients = await clientService.GetClientsAsync();
        }
    }
}
