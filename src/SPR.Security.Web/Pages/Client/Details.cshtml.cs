using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Interactions;

namespace SPR.Security.Web.Pages.Client
{
    public class DetailsModel : PageModel
    {
        private readonly IInteractionsService interationsService;
        private readonly IClientCRUDService clientService;

        public DetailsModel(IClientCRUDService clientService, IInteractionsService interationsService)
        {
            this.interationsService = interationsService ?? throw new ArgumentNullException(nameof(interationsService));
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        public ClientDataModel Client { get; private set; }

        public IList<TokenModel> Tokens { get; private set; }

        public IList<VerifyHistoryModel> VerifyHistory { get; private set; }

        public IList<WarningModel> Warnings { get; private set; }

        public async Task OnGetAsync(int id)
        {
            Client = await clientService.GetClientAsync(id);            
            Tokens = await interationsService.LoadTokensAsync(id, null, 10);
            Warnings = await interationsService.LoadWarningsAsync(id, null, 10);
            VerifyHistory = await interationsService.LoadHistoryAsync(id, null, 10);
        }
    }
}
