using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;

namespace SPR.Security.Web.Pages.Client
{
    public class EditModel : PageModel
    {
        private readonly IClientCRUDService clientService;

        public EditModel(IClientCRUDService clientService)
        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
        }

        [BindProperty]
        public ClientDataModel Client { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Client = await clientService.GetClientAsync(id);
            if (Client == null)
                return NotFound();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();
            
            var result = await clientService.UpdateAsync(Client);
            if (!result)
                return NotFound();

            return RedirectToPage(new { id = Client.Id });
        }
    }
}
