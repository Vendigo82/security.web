﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Interactions;
using SPR.Security.Web.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Security.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IInteractionsService service;
        private readonly IOptions<PageSettings<IndexModel>> settings;

        public IndexModel(IInteractionsService service, IOptions<PageSettings<IndexModel>> settings)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public IEnumerable<ClientSummaryModel> Clients { get; private set; }
        public IEnumerable<WarningModel> Warnings { get; private set; }

        public async Task OnGetAsync()
        {
            Clients = await service.LoadSummaryAsync();
            Warnings = await service.LoadWarningsAsync(null, null, settings.Value.ItemsPerPage);
        }
    }
}
