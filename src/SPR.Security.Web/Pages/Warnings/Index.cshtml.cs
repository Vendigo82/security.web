using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.DataModel.Clients;
using SPR.Security.Web.Services.DataModel.Interactions;
using SPR.Security.Web.Settings;

namespace SPR.Security.Web.Pages.Warnings
{
    public class IndexModel : PageModel
    {
        private readonly IInteractionsService service;
        private readonly IClientCRUDService clientService;
        private readonly IOptions<PageSettings> settings;

        public IndexModel(IInteractionsService service, IClientCRUDService clientService, IOptions<PageSettings> settings)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public IEnumerable<WarningModel> Items => Enumerable.Empty<WarningModel>();
        public ClientDataModel Client { get; private set; }
        public long? ClientId { get; private set; }

        public async Task OnGetAsync([FromQuery] long? clientId)
        {
            if (clientId != null)
                Client = await clientService.GetClientAsync(clientId.Value);
            ClientId = clientId;
        }

        public async Task<IActionResult> OnGetListAsync([FromRoute] long? id,  [FromQuery] long? clientId)
        {
            var list = await service.LoadWarningsAsync(clientId, id, settings.Value.ItemsPerPage);
            return Partial("_WarningsTableRows", (list.AsEnumerable(), list.Count < settings.Value.ItemsPerPage, clientId));
        }
    }
}
