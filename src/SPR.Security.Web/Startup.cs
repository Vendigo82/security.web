using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SPR.Security.Web.DAL.Model;
using SPR.Security.Web.Services;
using SPR.Security.Web.Services.Implementations;
using SPR.Security.Web.Services.Mapping;
using SPR.Security.Web.Settings;
using System;

namespace SPR.Security.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment host)
        {
            Configuration = configuration;
            Host = host;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Host { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<PageSettings>(Configuration);
            services.Configure<PageSettings<Pages.IndexModel>>(Configuration.GetSection("Main"));

            services.AddDbContext<GuardContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Guard")));

            services.AddRazorPages();
            services.AddAutoMapper(typeof(EFMapping));

            services
                .AddAuthentication(options => {
                    options.DefaultScheme = "Cookies";
                    options.DefaultChallengeScheme = "oidc";
                })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options => {
                    options.Authority = Configuration["OpenIdConnect:Authority"];
                    options.RequireHttpsMetadata = !Host.IsDevelopment();

                    options.ClientId = "guard_web";
                    options.ClientSecret = "monkeyseverywhere";
                    options.ResponseType = "code";

                    options.SaveTokens = true;
                    options.UseTokenLifetime = true;

                    options.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(0);

                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.Scope.Add("offline_access");
                });

            services.ConfigureNonBreakingSameSiteCookies();

            services.AddTransient<IClientCRUDService, ClientCRUDService>();
            services.AddTransient<IInteractionsService, InteractionsService>();
            services.AddTransient<IErrorsService, ErrorsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapRazorPages().RequireAuthorization();
            });
        }
    }
}
