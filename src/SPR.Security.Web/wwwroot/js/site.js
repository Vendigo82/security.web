﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function loadRows(refId, tableId, loadingUrl) {
    var $link = $('#' + refId + '');
    var click = $link.attr('onclick');
    $link.attr('onclick-data', click).removeAttr('onclick');
    $.ajax({
        url: loadingUrl,
        complete: function (data) {
            $('#' + tableId + ' tr:last').remove();
            $('#' + tableId + ' tr:last').after(data.responseText);
        }, error: function (data) {
            console.log('Failed to load data: status code ' + data.statusCode + '; ' + data.responseText);
            var $link = $('#' + refId + '');
            var click = $link.attr('onclick-data');
            $link.attr('onclick', click).removeAttr('onclick-data');
        }
    });
}

function onLoadedComplete() {    
}