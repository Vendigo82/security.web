﻿
namespace SPR.Security.Web.Settings
{
    public class PageSettings
    {
        public int ItemsPerPage { get; set; }
    }    

    public class PageSettings<T> : PageSettings
    {

    }
}
